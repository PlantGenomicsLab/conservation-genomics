# Conservation Genomics Exercises

#### **1. Identify software packages that are most frequently used on the Xanadu cluster** 

_Hint: Use grep, awk, and sed_

a. Copy `cluster_software_usage.txt` into your working directory
<pre>
/core/labs/Wegrzyn/Homework/week_6/cluster_software_usage.txt
</pre>

<details><summary>Answer</summary>

`cp /core/labs/Wegrzyn/Homework/week_6/cluster_software_usage.txt .`
</details>



b. Strip the version number from each software

_Example:_
<pre>
5630966|BioNetGen/2.5.0 => 5630966|BioNetGen
</pre>
- 5630966 = number of uses
- BioNetGen = software
- 2.5.0 = version number

<details><summary>Answer</summary>

`sed 's|/.*||' cluster_software_usage.txt > no_version.txt`
</details>


c. Count the unique instances of software and maintain order in the file list

_Example: zlib occurs four times in the list_

<pre>
3216|zlib/1.2.11 => 3216|zlib
1|zlib/1.2.11xanadu-03 => 1|zlib
1|zlib/1.2.11xanadu-50 => 1|zlib
1|zlib/1.2.11xaxanadu-15 => 1|zlib
</pre>
_and is used 3219 (3216+1+1+1) times._

<pre>
3219 zlib 
</pre>

<details><summary>Answer</summary>

`awk -F '|' '{ count[$2] += $1 } END { for (software in count) print count[software] "\t" software }' no_version.txt | sort -rn > unique_software.txt`
</details>

#### **2. Change FASTA headers** 
a. Copy `Glycine_max.fasta` into your home directory
<pre>
/core/labs/Wegrzyn/Homework/week_6/Glycine_max.fasta
</pre>

_Hint: You can check how many sequences are in your fasta file by using_ `grep -c ">"` _command!_


b. This protein file was downloaded from **Phytozome.org**. The sequence headers in this file look something like this:
<pre>
>Glyma.16G009500.2.p pacid=41077002 transcript=Glyma.16G009500.2 locus=Glyma.16G009500 ID=Glyma.16G009500.2.Wm82.a4.v1 annot-version=Wm82.a4.v1
</pre>

Too long! Change the headers in `Glycine_max.fasta` so they display just the **name** or **unique identifier** of the protein sequences.
<br>

_Example_:
<br>

Go from this:
<pre>
>Glyma.16G009500.2.p pacid=41077002 transcript=Glyma.16G009500.2 locus=Glyma.16G009500 ID=Glyma.16G009500.2.Wm82.a4.v1 annot-version=Wm82.a4.v1
</pre>

...to this!
<pre>
>Glyma.16G009500.2.p
</pre>

<details><summary>Answer</summary>

`sed '/^>/s/ .*//' Glycine_max.fasta > Glycine_max_modified.fasta`
or
`awk '/^>/ {$0=$1} 1'`

</details>



